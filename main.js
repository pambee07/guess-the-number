window.addEventListener("DOMContentLoaded", function (event) {
  // Initialisation des variables
  let tries;
  let randomNumber;

  const form = document.getElementById("guess_form");
  const resultElement = document.getElementById("result");
  const scoreElement = document.getElementById("score");
  const previousScore = window.localStorage.getItem("score");
  console.log("Score précédent : " + previousScore);
  scoreElement.textContent = previousScore;
  function response(form, message) {
    resultElement.textContent = message + " Score: " + previousScore;
    //let b = document.body;
    let b = form;
    let newP = document.getElementById("message");
    //let newTexte = document.createTextNode("Texte écrit en JavaScript");
    //newP.textContent = "Paragraphe créé et inséré grâce au JavaScript";
    newP.textContent = message;

    //Ajoute le paragraphe créé comme premier enfant de l'élément body

    //Ajoute le texte créé comme dernier enfant de l'élément body
    //b.append(newTexte);
  }
  // Définition des fonctions
  // Fonction qui traite la réponse du formulaire
  function submitForm(event) {
    // on evité le rechargement de la page
    event.preventDefault();

    tries++;
    console.log("Tour #" + tries);
    const message = "Tour #" + tries;
    response(form, message);

    // on affiche le nombre saisie
    const value = form.guess.value;
    console.log("Le nombre saisi est :" + value + "-" + randomNumber);

    if (value == randomNumber) {
      console.log("Bravo");
      const message = "Bravo";
      window.localStorage.setItem("score", tries);

      response(form, message);
      startGame();
    } else if (value < randomNumber) {
      console.log("c'est plus!");
      const message = "c'est plus!";
      response(form, message);
    } else if (value > randomNumber) {
      console.log("c'est moins!");
      const message = "c'est moins!";
      response(form, message);
    } else {
      console.log("error ce ne sont pas deux numbers");
    }
  }

  // Fonction qui commence le jeu
  function startGame() {
    tries = 0;
    randomNumber = Math.floor(Math.random() * 100);

    // Je rajoute ici un console.log() qui permet d'afficher le nombre qui a été tiré au hasard
    console.log("Le nombre à deviner est :" + randomNumber);
  }

  startGame();
  form.addEventListener("submit", submitForm);
});
